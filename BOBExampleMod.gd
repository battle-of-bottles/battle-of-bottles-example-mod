extends Node


func _ready():
	print("Hello World")

func _process(delta):
	# Multiplayer is a singleton of the main game, the error can be ignored
	if Multiplayer.connected:
		rpc("hello", "Hello from Client")

# hello must be handled, otherwise the game will throw errors
remote func hello(message):
	pass
