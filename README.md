# Battle of Bottles example mod

This is an example mod for Battle of Bottles which prints "Hello World" to the Godot console. If a client connects to the server it will send a "message" repeatedly to the server which the server will print to the Godot console.

### How does this work?
Battle of Bottles uses Godot's built-in [resource packs system](https://docs.godotengine.org/en/stable/getting_started/workflow/export/exporting_pcks.html) 
for simple and powerful modding support. The mods will be imported at run-time and replace files with the same path/name. Additionally, a main scene can
be provided that will be automatically added as a child of the root node (In this case, bob-example-mod.tscn). This scene should be called the same as the
actual mod file (e.g. "example.tscn" as main scene in "example.pck"). If there is a server scene ending with "_server.tscn" (e.g. "example_server.tscn"), the [Server](https://gitlab.com/battle-of-bottles/battle-of-bottles-server) will prefer that scene over the normal scene. This can be used for defining different behaviour depending on whether the mod is loaded by the server or by the client.

To let the Game properly load the mod and show it in the mod settings you have to provide a manifest file. The manifest file contains name and version (see "bob-example-mod.json"). It follows the same naming rule as the default (client-side) mod scene (but ends with ".json" instead of ".tscn").

### Exporting
Open the project in the [Godot editor](https://godotengine.org/). Go to **Project > Export** and add an export preset. Go to the "Resources" tab and add
"*.json" as a non-resource file that should be exported. Finally, press **Export PCK/Zip** and give the mod a .pck file extension.